# lumosstick.py (LumosStick)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython library supports the LumosStick hardware.

Implementation Notes
--------------------
**Hardware:**
    * the LumosStick (available on Tindie) https://www.tindie.com/stores/bradanlane/

**Software and Dependencies:**
    * CircuitPython firmware (UF2): https://circuitpython.org/board/lolin_s2_mini/
    * Adafruit CircuitPython Libraries: https://circuitpython.org/libraries
"""

import supervisor
import board
import time
import digitalio
import pwmio
import keypad as keypad_module
import neopixel
import ssl
import wifi
import socketpool

"""
    The wifi credentials and Adafruit IO credentials are stored is a file called secrets.py
    A template of this file is included with the project.
"""
try:
    from secrets import secrets
except ImportError:
    secrets = None
    pass

try:
    from adafruit_bitmap_font import bitmap_font
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/adafruit_bitmap_font'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")

try:
    from adafruit_datetime import datetime, timedelta
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/'adafruit_datetime.pmy'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")

try:
    import adafruit_requests
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/'adafruit_requests.pmy'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")
    adafruit_requests = None

try:
    import adafruit_ntp
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/adafruit_ntp'")
    print ("The Adafruit CircuitPython Libraries bundle is available from:")
    print ("https://circuitpython.org/libraries")
    adafruit_ntp = None



print ("")

class _Buttons:

    """
        the Buttons class manages both buttons
        Buttons support two states: PRESSED and RELEASED
    """

    def __init__(self):
        self._buttons = keypad_module.Keys((board.IO11,board.IO12,), value_when_pressed=False, pull=True)
        self.event = keypad_module.Event()
        # https://docs.circuitpython.org/en/latest/shared-bindings/keypad/index.html
        # event has 'key_number' of 0 or 1, 'pressed' is True or False, also has timestamp

    def has_event(self):
        return self._buttons.events.get_into(self.event)


class _Buzzer:
    """
        The LumosStick has a simple piezo buzzer available for tones.
        Tones are generated using PWM.
        To generate a tone, provide a frequency, and a duration.

        example:
            tone(880, 0.5)
    """

    def __init__(self):
        _STACKSIZE = 6
        self._buzzer_power = digitalio.DigitalInOut(board.IO9)
        self._buzzer_power.direction = digitalio.Direction.OUTPUT
        self._buzzer_power.value = False
        self._pwm = None
        self._timer = 0


    def toneon(self, frequency=880):
        if self._pwm is None:
            self._buzzer_power.value = True
            self._pwm = pwmio.PWMOut(board.IO7, frequency=int(frequency), variable_frequency=True)
            self._pwm.duty_cycle = 0x8000
        else:
            self._pwm.frequency = frequency
        print("frequency = %d" % (self._pwm.frequency))

    def toneoff(self):
        if self._pwm is not None:
            self._pwm.deinit()
            self._pwm = None
            self._buzzer_power.value = False


    def tone(self, frequency=880, seconds=0.5, wait=True):
        self.toneon(frequency)        
        if wait:
            time.sleep(seconds)
            self.toneoff()
        else:
            self._timer = supervisor.ticks_ms() + int(1000 * seconds)


    def update(self):
        if self._timer:
            if self._timer < supervisor.ticks_ms():
                self._timer = 0
                self.toneoff()


class _Neopixels:
    """
        The LumosStick has a large number of WS2812C-2020 addressable LEDs.
        These LEDs are often referred to as NeoPixels.
        The are connected in 7 sequences. Each sequence contains 40 LEDs.
    """
    """
        source of many fonts: https://github.com/olikraus/u8g2/tree/master/tools/font/bdf
        some fonts may cause an error. the most common two problems are:
            1) there 'copyright' text
            2) the bounding box is in the form of a string rather than four values
        BDF font format may be converted to PCF format using the following online tool:
            https://adafruit.github.io/web-bdftopcf/
    """


    def __init__(self):
        try:
            fontname = 'font/5x7.pcf'
            self._font = bitmap_font.load_font(fontname)
        except:
            print("Warning: Missing '%s'" % (fontname))
            print("The font is located in the project 'font' folder on gitlab")
            self._font = None
            pass

        self._brightness = 0.25

        self._row1 = neopixel.NeoPixel(board.IO33, 40, brightness=self._brightness, auto_write=False)
        self._row2 = neopixel.NeoPixel(board.IO34, 40, brightness=self._brightness, auto_write=False)
        self._row3 = neopixel.NeoPixel(board.IO35, 40, brightness=self._brightness, auto_write=False)
        self._row4 = neopixel.NeoPixel(board.IO36, 40, brightness=self._brightness, auto_write=False)
        self._row5 = neopixel.NeoPixel(board.IO37, 40, brightness=self._brightness, auto_write=False)
        self._row6 = neopixel.NeoPixel(board.IO38, 40, brightness=self._brightness, auto_write=False)
        self._row7 = neopixel.NeoPixel(board.IO39, 40, brightness=self._brightness, auto_write=False)
        self._rows = (self._row1, self._row2, self._row3, self._row4, self._row5, self._row6, self._row7)

        self.block_fill(LumosStick.BLACK)
        self._block_dirty = True
        self.show()

    @property
    def brightness(self):
        return self._brightness
    @brightness.setter
    def brightness(self, level):
        level = level if level >= 0 else 0
        level = level if level < 0.5 else 0.5
        level = round(level, 2)
        self._brightness = level
        for i in range(7):
            self._rows[i].brightness = level
        self._block_dirty = True

    def block_fill(self, color):
        for i in range(7):
            self._rows[i].fill(color)
        self._block_dirty = True

    def block_glyph_fill(self, dx, color):
        """
            the block neopixels are arranged as a series of rows, each row containing 10 neopixels
            if you imagine the block as a wide 10x7 rectangle, then the first neopixel is the left top and
            the last neopixel is the right bottom.
        """
        for y in range(7):
            for x in range(6):
                if ((x + dx) >= 0) and ((x + dx) < 40) and (y < 7):
                    self._rows[y][dx + x] = color
                else:
                    #print("glyph_fill(%d) at [%2d,%2d]" % (dx, dx+x, y))
                    pass
        self._block_dirty = True

    def block_pixel(self, x, y, color):
        """
            the block neopixels are arranged as a series of rows, each row containing 10 neopixels
            if you imagine the block as a wide 10x7 rectangle, then the first neopixel is the left top and
            the last neopixel is the right bottom.
        """
        self._rows[y][x] = color
        self._block_dirty = True

    def _render_glyph(self, start_x, glyph, color=0x3f3f3f, background=0x000000):
        """
        a glyph is an X*Y bitmap up to 5x7 but its height may not be the full 7 rows
        a glyph has width, height, and an offset for both x and y
        the glyph has an origin (0,0) upper left; the bitmap starts at the offset (dx, dy)

        """
        #print("char=%s [%02,%2d,%2d,%2d]" % (chr(val), glyph.width, glyph,height, glyph.dx, glyph.dy), glyph)    # debugging

        """ erase space used by the glyph; this is somewhat inefficient as we erase everything even if we will later also overwrite most of it
            an optimization is to ...
                erase anything before and/or above the actual glyph bitmap
                then render the glyph
                then erase anything remaining after and/or below the bitmap
        """

        # if the entire 5x7 glyph is off the left edge of of the LEDs, we can skip it
        if (start_x + 5) < 0:
            return

        #if (glyph.width < 5) or (glyph.height < 7):
        self.block_glyph_fill(start_x, background)
                    
        # render the glyph bitmap
        dx = glyph.dx + start_x
        dy = (7 - glyph.height) if (glyph.dy == 0) else glyph.dy
        #print("glyph [%2d %2d  %2d %2d]" % (glyph.width, glyph.height, glyph.dx, glyph.dy))

        for y in range(glyph.height):
            for x in range(glyph.width):
                bit = glyph.bitmap[x, y]
                if ((x + dx) >= 0) and ((x + dx) < 40) and ((y + dy) < 7):
                    self._rows[y+dy][x+dx] = (color if (bit & 0x1) else background)


    def block_skinny_colon(self, start_x, color=0x3f3f3f, background=0x000000):
        # this is primarily a cheater method for adding a narrow colon between clock digits
        self._rows[0][start_x] = background
        self._rows[1][start_x] = color
        self._rows[2][start_x] = color
        self._rows[3][start_x] = background
        self._rows[4][start_x] = color
        self._rows[5][start_x] = color
        self._rows[6][start_x] = background


    def block_digits(self, start_x, number, length, color=0x3f3f3f, background=0x000000):
        if (self._font is None) or (length <= 0):
            return

        number = number % (10 ** length)
        # for each digit
        for i in range((length * 6), 0, -6):
            dx = i - 6
            n = number % 10     # left most digit
            n += 48             # get ascii index
            glyph = self._font.get_glyph(n)
            if not glyph:
                continue
            self._render_glyph(start_x + dx, glyph, color, background)
            number = int(number / 10)
        self._block_dirty = True


    def block_two_digits(self, start_x, number, color=0x3f3f3f, background=0x000000):
        self.block_digits(start_x, number, 2, color, background)


    def block_letters(self, start_x, text, color=0x3f3f3f, background=0x000000):
        if self._font is None:
            return

        text_array = list(text)
        index = 0
        # for each character
        dx = 0
        while (start_x + dx) < 40:
            n = ord(text_array[index])
            glyph = self._font.get_glyph(n)
            if not glyph:
                continue
            self._render_glyph(start_x + dx, glyph, color, background)
            index += 1
            dx += 6
            if index >= len(text):
                break
        self._block_dirty = True


    def update(self):
        pass    # currently nothing to do 

    def show(self):
        if self._block_dirty:
            for i in range(7):
                self._rows[i].show()
                self._block_dirty = False


class _Clock:
    NOCLOCK = 0
    CLOCK = 1
    TIMER = 2
    COUNTDOWN = 3

    _TIME_NONE = 0
    _TIME_ADAFRUIT = 1
    _TIME_WORLD = 2
    _TIME_NTP = 3

    _ONE_SECOND = timedelta(seconds=1)   # a constant used for updating the clock
    _MAX_ATTEMPTS = 6

    def __init__(self, mode=NOCLOCK):

        self._datetime = datetime(1970, 1, 1, 0, 0, 0)
        self._time = time.time()

        self._synched = False
        self._sync_attempts = 0
        self._dirty = True

        # public properties
        self.stopped = False if mode == _Clock.CLOCK else True
        self.hour = self._datetime.hour
        self.minute = self._datetime.minute
        self.second = self._datetime.second
        self.mode = mode
        self.manual = False if mode == _Clock.CLOCK else True

        self._blink = digitalio.DigitalInOut(board.IO15)
        self._blink.direction = digitalio.Direction.OUTPUT
        self._blink.value = False

        self.resync()

    def resync(self):
        if self.manual or (self.mode != _Clock.CLOCK):
            return

        print ("Updating time: %02d:%02d:%02d" % (self.hour, self.minute, self.second))
        success = True
        wifi_active = False
        response = None

        if secrets is None:
            print("Error: Missing 'secrets.py'")
            print("    this file contains the WiFi credentials")
            print("    and the optional Adafruit IO credentials")
            print("    copy secrets_empty.py to secrets.py")
            print("    edit the JSON with your information")
            print("    then copy secrets.py to the 'CIRCUITPY' device")
            print("    for more information on Adafruit IO")
            print("    visit: https://io.adafruit.com")
            success = False

        if success:
            ssid = secrets.get("ssid", None)
            pw = secrets.get("password", None)
            username = secrets.get("aio_username", None)
            key = secrets.get("aio_key", None)
            ntp_server = secrets.get("ntp_server", None)
            ntp_offset = secrets.get("ntp_offset", 0)
            location = secrets.get("timezone", None)

            if ssid is None:
                print("Error: missing 'ssid' in 'secrets.py' file.")
                success = False

        # attempt to connect to WiFi
        if success:
            wifi_active = True
            wifi.radio.enabled = True
            print("Connecting to '%s'" % secrets["ssid"])

            try:
                wifi.radio.connect(secrets["ssid"], secrets["password"])
            except ConnectionError as e:
                print("------------------------------------------------------------------------")
                print(e)
                print("------------------------------------------------------------------------")
                print("Unable to connect to '%s' using '%s'" % (secrets["ssid"], secrets["password"]))

                # flash the onboard LED
                for i in range(8):
                    self._blink.value = True
                    time.sleep(0.25)
                    self._blink.value = False
                    time.sleep(0.25)

                print("Available WiFi networks:")
                for network in wifi.radio.start_scanning_networks():
                    print("\t'%s'\t\tRSSI: %d\tChannel: %d" % (str(network.ssid, "utf-8"),
                            network.rssi, network.channel))
                wifi.radio.stop_scanning_networks()

                wifi.radio.enabled = False
                wifi_active = False
                success = False
            # end of exception when attempting to connect to wifi

        # we have connected to WiFi
        if success:
            print("Connected  to '%s'" % secrets["ssid"])
            print("My IP address is", wifi.radio.ipv4_address)
            self._blink.value = True
            pool = socketpool.SocketPool(wifi.radio)


        # attempt to access internet time service (AdafruitIO or NTP)
        svc = _Clock._TIME_NONE
        if adafruit_requests is not None:
            svc = _Clock._TIME_ADAFRUIT
        elif adafruit_ntp is not None:
            svc = _Clock._TIME_NTP

        # check if we have sufficient parameters for AdafruitIO
        if success and (svc == _Clock._TIME_ADAFRUIT):
            if username is None:
                # print("Error: missing 'aio_username' in 'secrets.py' file.")
                svc = _Clock._TIME_WORLD # try WORLD next
            if key is None:
                # missing secrets.py file or required fields
                # set a generic time and mark it as synced since we will never get a real clock
                # print("Error: missing 'aio_username' in 'secrets.py' file.")
                svc = _Clock._TIME_WORLD # try WORLD next

        # we have AdafruitIO parameters
        if success and (svc == _Clock._TIME_ADAFRUIT):
            requests = adafruit_requests.Session(pool, ssl.create_default_context())
            TIME_URL = "https://io.adafruit.com/api/v2/%s/integrations/time/strftime?x-aio-key=%s" % (username, key)
            TIME_URL  += "&fmt=%25Y-%25m-%25d+%25H%3A%25M%3A%25S"
            if location:
                TIME_URL += "&tz=%s" % (location)

            response = requests.get(TIME_URL)

            try:
                self._datetime = datetime.fromisoformat(response.text)
                self._synched = True
                self._sync_attempts  = 0
            except ValueError:
                print("Error: Unable to parse response from Adafruit IO service:")
                print("       Service URL: %s" % (TIME_URL))
                print("       Response: %s" % (response.text))
                svc = _Clock._TIME_NONE
                success = False # if the user wants AdafruitIO and we fail, we don't attempt NTP


        # check if we have sufficient parameters for WorldTimeAPI
        if success and (svc == _Clock._TIME_WORLD):
            if location is None:
                # print("Error: missing 'aio_username' in 'secrets.py' file.")
                svc = _Clock._TIME_NTP # try NTP next

        # we have WorldTimeAPI parameter
        if success and (svc == _Clock._TIME_WORLD):
            requests = adafruit_requests.Session(pool)
            TIME_URL = "http://worldtimeapi.org/api/timezone/%s" % (location)

            response = requests.get(TIME_URL)

            try:
                json_response = response.json()
                self._datetime = datetime.fromisoformat(json_response['datetime'])
                self._synched = True
                self._sync_attempts  = 0
            except ValueError:
                print("Error: Unable to parse response from WorldTimeAPI.org:")
                print("       Service URL: %s" % (TIME_URL))
                print("       Response: %s" % (response.text))
                svc = _Clock._TIME_NONE
                success = False # if the user wants AdafruitIO and we fail, we don't attempt NTP


        # we don't have AdafruitIO or WORLD then we will try NTP ...  but only if we have the NTP library
        if success and (svc == _Clock._TIME_NTP) and (adafruit_ntp is None):
            svc = _Clock._TIME_NONE
            success = False

        # we don't have AdafruitIO so we will try NTP
        if success and (svc == _Clock._TIME_NTP):
            if ntp_server is None:
                print("Using default ntp server pool.ntp.org")
                print("To use a custom ntp server set 'ntp_server' in 'secrets.py' file.")
                ntp_server = "pool.ntp.org"
            try:
                ntp = adafruit_ntp.NTP(pool, server=ntp_server, tz_offset=ntp_offset)
                print("Getting time from ntp server %s" % ntp_server)
                now = ntp.datetime
                self._datetime = datetime(now.tm_year,now.tm_mon,now.tm_mday, now.tm_hour,now.tm_min,now.tm_sec)
                print("NTP time is %s" % (self._datetime))
                self._synched = True
                self._sync_attempts  = 0
            except OSError:
                print("Error: no response received from %s" % ntp_server)
                mode = _Clock._TIME_NONE
                success = False

        if response is not None:
            response.close()

        if wifi_active:
            wifi_active = False
            wifi.radio.enabled = False

        if not success:
            self._sync_attempts += 1
            if (not self._synched) or (self._sync_attempts > _Clock._MAX_ATTEMPTS):
                self._datetime = datetime(1970, 1, 1, 0, 0, 0)
                self._synched = False
                self.manual = True

        # end of our single-pass loop
        self._time = time.time()
        self.hour = self._datetime.hour
        self.minute = self._datetime.minute
        self.second = self._datetime.second
        print ("Time updated:  %02d:%02d:%02d" % (self.hour, self.minute, self.second))


    def update(self):
        if self._datetime is None:
            return
        if self.stopped == True:
            return
        # if countdown and we are at 0:0:0
        if self.mode == _Clock.COUNTDOWN:
            if (not self.hour) and (not self.minute) and (not self.second):
                self.stopped = True
                return

        if self._time != time.time():
            self._dirty = True
            self._time = time.time()
            self._datetime += _Clock._ONE_SECOND
            self.hour = self._datetime.hour
            self.minute = self._datetime.minute
            self.second = self._datetime.second

            if self.second == 0:
                if self.minute == 0:
                    # at 4, 12, 20 hours, we resync the clock with the internet
                    if (self.hour == 4) or (self.hour == 12) or (self.hour == 20):
                        self.resync()
                else:
                    # if we failed our last sync attempt, then we will try every 5 minutes for up to MAX_ATTEMPTS
                    if not self.manual:
                        if (self.minute % 5) and (not self._synched) and (self._sync_attempts <= _Clock._MAX_ATTEMPTS):
                            self.resync()


class LumosStick:

    # clock modes
    NOCLOCK = 0
    CLOCK = 1
    TIMER = 2
    COUNTDOWN = 3

    # time increments
    ONESECOND = timedelta(seconds=1)   # a constant used for updating the clock
    ONEMINUTE = timedelta(minutes=1)   # a constant used for updating the clock
    ONEHOUR = timedelta(hours=1)   # a constant used for updating the clock

    # buttons
    LEFT = 0
    RIGHT = 1

    # button states
    RELEASED = 0
    PRESSED = 1
    
    RED		= 0xff0000 #(255,  0,  0)
    GREEN	= 0x007f00 #(  0,127,  0)
    ORANGE  = 0xe04020 #(255, 93,  0)
    YELLOW	= 0xffff00 #(255,127,  0)
    BLUE	= 0x0000ff #(  0, 0, 255)
    MAGENTA	= 0xff00ff #(255,  0,255)
    PURPLE	= 0x3f007f #( 63,  0,127)
    PINK	= 0x7f007f #(127,  0,127)
    CYAN	= 0x00ffff #(  0,127,255)
    WHITE	= 0x7f7f7f #(127,127,127)
    GRAY	= 0x1f1f1f #( 31, 31, 31)
    GREY = GRAY
    DKGRAY	= 0x0f0f0f #( 15, 15, 15)
    BLACK	= 0x000000 #(  0,  0,  0)

    def __init__(self, mode=NOCLOCK):
        self._buzzer = _Buzzer()
        self._buttons = _Buttons()
        self._leds = _Neopixels()
        self._clock = _Clock(mode)

        self._background = LumosStick.BLACK
        # hours minutes, seconds, ticks, marks
        self._color = LumosStick.BLUE

        if (self._clock.mode == LumosStick.CLOCK) and self._clock.manual:
            # in manual mode, we give the user the chance to use the buttons to set the clock
            self.tone (523, 0.1)
            self.tone (494, 0.1)
            self.tone (523, 0.2)
            self._set_clock()


    def _set_clock(self):
        timer = supervisor.ticks_ms() + 15000 #  seconds to set the time manually
        left_timer = 0
        right_timer = 0
        while timer > supervisor.ticks_ms():
            do_left = False
            do_right = False
            if self.buttons_changed:
                button, action = self.button
                if button == LumosStick.LEFT:
                    if  action == True:
                        do_left = True
                        left_timer = supervisor.ticks_ms() + 750
                    else:
                        left_timer = 0
                if button == LumosStick.RIGHT:
                    if action == True:
                        do_right = True
                        right_timer = supervisor.ticks_ms() + 750
                    else:
                        right_timer = 0

            # support press-and-hold
            if left_timer and (left_timer < supervisor.ticks_ms()):
                do_left = True
                left_timer = supervisor.ticks_ms() + 125
            if right_timer and (right_timer < supervisor.ticks_ms()):
                do_right = True
                right_timer = supervisor.ticks_ms() + 125

            if do_left:
                self.tone (523, 0.1)
                if self._clock.mode == LumosStick.COUNTDOWN:
                    self._clock.hour -= 1
                    if self._clock.hour < 0:
                        self._clock.hour += 24
                else:
                    self._clock.hour += 1
                self._clock.hour = self._clock.hour % 24
                self._clock._dirty = True
                timer = supervisor.ticks_ms() + 10000 #  seconds to set the time manually
            if do_right:
                self.tone (494, 0.1)
                # left button press. advance hour
                if self._clock.mode == LumosStick.COUNTDOWN:
                    self._clock.minute -= 1
                    if self._clock.minute < 0:
                        self._clock.minute += 60
                else:
                    self._clock.minute += 1
                self._clock.minute = self._clock.minute % 60
                self._clock._dirty = True
                timer = supervisor.ticks_ms() + 10000 #  seconds to set the time manually

            if self._clock._dirty:
                print ("Time: %2d:%2d:%2d" % (self._clock.hour, self._clock.minute, self._clock.second))
                self._show()

        self._clock._datetime = datetime(1970, 1, 1, self._clock.hour, self._clock.minute, 0)
        # end of _set_clock()


    def _show(self):
        #start = supervisor.ticks_ms()
        if (self._clock.mode != _Clock.NOCLOCK) and self._clock._dirty:
            self._clock._dirty = False
            #print ("%02d:%02d:%02d" % (self._clock.hour, self._clock.minute, self._clock.second))
            self._leds.block_fill(self._background) # this wipes the entire string, we only want to wipe the first 240

            """
                render hours colon minutes colon seconds
            """

            start_x = 0     # left most position
            if not ((self._clock.mode == LumosStick.COUNTDOWN) and ((self._clock.hour == 0) or self._clock.hour == 23)):
                n = self._clock.hour # TODO support 12 and 24 hour clocks
                self._leds.block_two_digits(start_x, n, self._color, self._background)
                self._leds.block_skinny_colon(start_x + 12, self._color, self._background)
            start_x = 14    # "00:" two digits plus a colon
            n = self._clock.minute
            self._leds.block_two_digits(start_x, n, self._color, self._background)
            self._leds.block_skinny_colon(start_x + 12, self._color, self._background)
            start_x = 28    # "00:00:" two digits plus a colon plus two diggits plus a colon

            n = self._clock.second
            self._leds.block_two_digits(start_x, n, self._color, self._background)
                
        self._leds.show()
        #end = supervisor.ticks_ms()
        #if end - start > 5:
        #    print("slow show", end-start)

    def update(self):
        #start = supervisor.ticks_ms()
        self._buzzer.update()
        if self._clock.mode != LumosStick.NOCLOCK:
            self._clock.update()
        self._show()
        #end = supervisor.ticks_ms()
        #if end - start > 5:
        #    print("slow update", end-start)

    @property
    def mode(self):
        return self._clock.mode
    @mode.setter
    def mode(self, mode):
        if mode == self._clock.mode:
            return
        old_mode = self._clock.mode
        self._clock.mode = mode
        if mode == LumosStick.CLOCK:
            self._clock.resync()
            if self._clock.manual:
                self.set_clock()

    @property
    def background(self):
        return self._background
    @background.setter
    def background(self, color):
        self._background = color
        self._clock._dirty = True
    @property
    def color(self):
        return self._color
    @color.setter
    def color(self, color):
        self._color = color
        self._clock._dirty = True
    @property
    def brightness(self):
        return self._leds.brightness
    @brightness.setter
    def brightness(self, level):
        self._leds.brightness = level

    @property
    def time(self):
        return self._clock._datetime
    @time.setter
    def time(self, dt):
        if isinstance(dt, datetime):
            self._clock._datetime = dt
        else:
            h = 0
            m = 0
            s = 0
            if isinstance(dt, tuple):
                if len(dt) > 0:
                    h = abs(dt[0])
                if len(dt) > 1:
                    m = abs(dt[1])
                if len(dt) > 2:
                    s = abs(dt[2])
            elif isinstance(dt, int):
                h = int(dt / 3600)
                dt = dt % 3600
                m = int(dt / 60)
                dt = dt % 60
                s = dt
            else:
                print("Error: unknown data type assigned to LumosStick.time")

            h = h if h < 24 else 0
            m = m if m < 60 else 0
            s = s if s < 60 else 0
            self._clock._datetime = datetime(1970, 1, 1, h, m, s)
        # end input types
        self._clock.hour = self._clock._datetime.hour
        self._clock.minute = self._clock._datetime.minute
        self._clock.second = self._clock._datetime.second
        self._clock._dirty = True
    @property
    def hour(self):
        return self._clock.hour
    @hour.setter
    def hour(self, h):
        self._clock.hour = h
        self._clock._datetime = datetime(1970, 1, 1, self._clock.hour, self._clock.minute, 0)
        self._clock._dirty = True
    @property
    def minute(self):
        return self._clock.minute
    @minute.setter
    def minute(self, m):
        self._clock.minute = m
        self._clock._datetime = datetime(1970, 1, 1, self._clock.hour, self._clock.minute, 0)
        self._clock._dirty = True
    @property
    def second(self):
        return self._clock.second
    @second.setter
    def second(self, s):
        self._clock.second = s
        self._clock._datetime = datetime(1970, 1, 1, self._clock.hour, self._clock.minute, self._clock.second)
        self._clock._dirty = True

    @property
    def stopped(self):
        return self._clock.stopped
    def stop(self):
        self._clock.stopped = True
    def start(self):
        self._clock.stopped = False
        
    def leds(self, color=None):
        if color is None:
            color = self._color
        self._leds.block_fill(color)
    def led(self, x, y, color=None):
        if color is None:
            color = self._color
        self._leds.block_pixel(x, y, color)
    def digits(self, dx, number, length=2, color=None, background=None):
        if color is None:
            color = self._color
        if background is None:
            background = self._background
        self._leds.block_digits(dx, number, length, color, background)
    def letters(self, dx, text, color=None, background=None):
        if color is None:
            color = self._color
        if background is None:
            background = self._background
        self._leds.block_letters(dx, text, color, background)
    def colon(self, dx, color=None, background=None):
        if color is None:
            color = self._color
        if background is None:
            background = self._background
        self._leds.block_skinny_colon(dx, color, background)

    def tone(self, freq, len):
        self._buzzer.tone(freq, len)
    def toneon(self, freq):
        self._buzzer.toneon(freq)
    def toneoff(self):
        self._buzzer.toneoff()

    @property
    def buttons_changed(self): # returns a boolean True if there is new event data
        return self._buttons.has_event()
    @property
    def button(self):
        return self._buttons.event.key_number, self._buttons.event.pressed




"""
supervisor.ticks_ms() is a counter which wraps
to accomodate for the wrapping, we provide the following class

Code derived from:
https://docs.circuitpython.org/en/latest/shared-bindings/supervisor/
"""
class SimpleTimer:
    _TICKS_PERIOD = const(1<<29)
    _TICKS_MAX = const(_TICKS_PERIOD-1)
    _TICKS_HALFPERIOD = const(_TICKS_PERIOD//2)

    def __init__(self, interval=100):
        self._interval = interval
        self._milliseconds = 0
        self._running = False

    @property
    def interval(self):
        return self._interval
    @interval.setter
    def interval(self, interval):
        self._interval = interval

    def start(self, interval=None):
        if interval is None:
            interval = self._interval
        else:
            self._interval = interval
        self._milliseconds = supervisor.ticks_ms() + self._interval
        self._running = True
    def stop(self):
        self._milliseconds = 0
        self._running = False
    @property
    def expired(self):
        if self._running == False:
            return False
        # "Compute the signed difference between two ticks values, assuming that they are within 2**28 ticks"
        now = supervisor.ticks_ms()
        diff = (self._milliseconds - now) & _TICKS_MAX
        diff = ((diff + _TICKS_HALFPERIOD) & _TICKS_MAX) - _TICKS_HALFPERIOD
        if diff < 0:
            self.stop()
            return True
        return False
        
