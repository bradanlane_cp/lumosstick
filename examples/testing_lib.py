# code.py (LumosStick Test example)
# Copyright: 2023 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosStick LEDs, buttons, and tone buzzer.

The LumosStick hardware connects via USB and provides:
    - 280 Neopixel LEDs
        - 7 rows of 40 LEDs each
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosStick is powered by a Lolin ESP32-S2 which include Wifi connectivity.

Documentation: 
**Hardware:**
    * the LumosStick (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import supervisor
import random
import time

from LumosStick import LumosStick, SimpleTimer   # REQUIRED: import the LumosStick library

'''
try:
    from LumosStick import LumosStick, SimpleTimer   # REQUIRED: import the LumosStick library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/LumosStick.py'")
    print ("The latest LumosStick library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/LumosStick")
    print("------------------------------------------------------------------------")
'''

lumos = LumosStick(mode=LumosStick.NOCLOCK)
lumos.tone(440,0.5)
lumos.brightness = 0.15

lumos.leds(LumosStick.WHITE)
lumos.clock_background = LumosStick.RED
#lumos.letters("XK", LumosStick.WHITE, LumosStick.RED)

colors = (LumosStick.RED, LumosStick.GREEN, LumosStick.BLUE, LumosStick.ORANGE, LumosStick.YELLOW, LumosStick.MAGENTA, LumosStick.PURPLE, LumosStick.PINK, LumosStick.CYAN, LumosStick.WHITE, LumosStick.GRAY, LumosStick.DKGRAY, LumosStick.BLACK)
MAX_COLORS = 13
color = 0

timer = SimpleTimer(1000)
timer.start()

counter = 0

while True:
    # we randomize eye movement (front is always between other positions)
    lumos.update()

    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosStick.RIGHT) and (action == LumosStick.PRESSED):
            lumos.tone(880,0.1)
            lumos.brightness += 0.05
            print("Right Button Increase Brightness to %f" % (lumos.brightness))
        if (button == LumosStick.LEFT) and (action == LumosStick.PRESSED):
            lumos.tone(220,0.1)
            lumos.brightness -= 0.05
            print("Left Button Decrease Brightness to %f" % (lumos.brightness))
    if timer.expired:
        timer.start()
        #lumos.letters("--", colors[(color % MAX_COLORS)], colors[((color+1) % MAX_COLORS)])
        color = (color + 1) % MAX_COLORS
        lumos.leds(colors[color])
        counter = (counter + 1) % 4
        lumos.toneon((110 * (counter + 1)))
        print("Changing color to %d. New Tone %d. Restarting timer" % (color, (110* (counter+1))))

# end of while True loop
