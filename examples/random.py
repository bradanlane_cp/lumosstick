# code.py (LumosStick Scrolling Text example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosStick LEDs, buttons, and tone buzzer.

The LumosStick hardware connects via USB and provides:
    - 280 Neopixel LEDs (when held horizontal it is 7 rows of 40 LEDs each)
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosStick is powered by a Lolin ESP32-S2 which include Wifi connectivity.

The sample code scrolls a static text string.

Documentation: 
**Hardware:**
    * the LumosStick (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import supervisor
import random
import time

try:
    from LumosStick import LumosStick, SimpleTimer   # REQUIRED: import the LumosStick library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/lumosstick.py'")
    print ("The latest LumosStick library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/lumosstick")
    print("------------------------------------------------------------------------")


lumos = LumosStick(mode=LumosStick.NOCLOCK)
brightness = 0.1
lumos.brightness = brightness
lumos.color = 0x5FFF5F

WAIT = 50   # milliseconds; this is about as fast as it can go
FOUND_SHORT =  2000
FOUND_LONG  = 15000
random.seed(supervisor.ticks_ms())

timer1 = SimpleTimer(WAIT)
timer1.start()

timer2 = SimpleTimer(random.randrange(FOUND_SHORT, FOUND_LONG))
timer2.start()

offset = 2
# make a random code
message = list((''.join((random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')) for x in range(6))))
found = [False, False, False, False, False, False]

# IMPORTANT: the text will look a bit jerky until all of the characters from the font have been loaded

while True:
    lumos.update()

    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosStick.RIGHT) and (action == LumosStick.PRESSED):
            brightness += 0.05
            if brightness > 0.3:
                brightness = 0.3
            lumos.brightness = brightness
        if (button == LumosStick.LEFT) and (action == LumosStick.PRESSED):
            brightness -= 0.05
            if brightness < 0:
                brightness = 0
            lumos.brightness = brightness

    # periodically 'solve' one of the letters
    if timer2.expired:
        timer2.interval = random.randrange(FOUND_SHORT, FOUND_LONG)
        timer2.start()

        i = random.randrange(6)
        while found[i]:
            i = random.randrange(6)
        found[i] = True
        lumos.tone(220, 0.25)

    all = False
    # update the screen
    if timer1.expired:
        timer1.start()

        text = ''
        all = True
        for i in range(6):
            if found[i]:
                text = text + text.join(message[i])
            else:
                text = text + text.join(random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'))
                all = False

        lumos.letters(offset, text)
        lumos.update()

        # once the code has been found, sound the alarm and the reset the system
        if all:
            lumos.letters(offset, "LAUNCH", color=LumosStick.RED)
            for i in range(6):
                lumos.tone(440, 0.25)
                lumos.brightness = 0
                lumos.update()
                lumos.tone(110, 0.25)
                lumos.brightness = brightness
                lumos.update()

                found[i] = False
                message[i] = random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')

            lumos.leds(LumosStick.BLACK)
            lumos.update()
            lumos.brightness = brightness

            time.sleep(0.5)
            # make a new random code



# end of while True loop
