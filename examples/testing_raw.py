# code.py (LumosRing Animation example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosRing LEDs, buttons, and tone buzzer.

The LumosRing hardware connects via USB and provides:
    - 310 Neopixel LEDs
        - ring of 240 LEDs as 60 lines of 4 LEDs each
        - block of 10x7 LEDs as 7 lines of 10 LEDs each
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosRing is powered by a Lolin ESP32-S2 which include Wifi connectivity.

This example demonstrates using the popular Adafruit libraries directly without the use of the LumosRing library

Documentation: 
**Hardware:**
    * the LumosRing (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""


import supervisor
import time
import board
import digitalio

import pwmio

power = digitalio.DigitalInOut(board.IO9)
power.direction = digitalio.Direction.OUTPUT
power.value = True
pwm = None

def tone_on(frequency):
    global pwm

    power.value = True

    if pwm is None:
        pwm = pwmio.PWMOut(board.IO7, frequency=440, variable_frequency=True)
        pwm.duty_cycle = 0x8000
        print("new tone %d" % (440))
    else:
        pwm.frequency = frequency
        print("new tone %d" % (frequency))

def tone_off():
    global pwm
    pwm.deinit()
    pwm = None

import neopixel

RED = (255, 0, 0)
YELLOW = (255, 150, 0)
GREEN = (0, 255, 0)
CYAN = (0, 255, 255)
BLUE = (0, 0, 255)
PURPLE = (180, 0, 255)
WHITE = (255, 255, 255)

colors = (WHITE, RED, GREEN, BLUE)
color = 0
timer = supervisor.ticks_ms() + 1000


row1 = neopixel.NeoPixel(board.IO33, 40, brightness=0.3, auto_write=False)
row2 = neopixel.NeoPixel(board.IO34, 40, brightness=0.3, auto_write=False)
row3 = neopixel.NeoPixel(board.IO35, 40, brightness=0.3, auto_write=False)
row4 = neopixel.NeoPixel(board.IO36, 40, brightness=0.3, auto_write=False)
row5 = neopixel.NeoPixel(board.IO37, 40, brightness=0.3, auto_write=False)
row6 = neopixel.NeoPixel(board.IO38, 40, brightness=0.3, auto_write=False)
row7 = neopixel.NeoPixel(board.IO39, 40, brightness=0.3, auto_write=False)

left  = digitalio.DigitalInOut(board.IO11)
right = digitalio.DigitalInOut(board.IO12)
left.direction  = digitalio.Direction.INPUT
right.direction = digitalio.Direction.INPUT
left.pull  = digitalio.Pull.UP
right.pull = digitalio.Pull.UP

left_was_pressed = (left.value == False)
right_was_pressed = (right.value == False)

brightness = 0.15

def change_leds():
    global color, colors
    print ("color change to %d" % (color))
    c = colors[color]
    row1.fill(c)
    row1.show()
    row2.fill(c)
    row2.show()
    row3.fill(c)
    row3.show()
    row4.fill(c)
    row4.show()
    row5.fill(c)
    row5.show()
    row6.fill(c)
    row6.show()
    row7.fill(c)
    row7.show()
    color = (color + 1) % 4
    tone_on(110*(color + 1))

while True:
    if timer < supervisor.ticks_ms():
        timer = supervisor.ticks_ms() + 1000
        change_leds()

    left_is_pressed = (left.value == False)
    right_is_pressed = (right.value == False)

    if (not left_was_pressed) and (left_is_pressed):
        print ("left pressed")
        # the left button has been pressed, we could do something ... like change brightness
        brightness = brightness - 0.05
        if brightness < 0.0:
            brightness = 0.0
        print ("new brightness = %f" % (brightness))
        row2.brightness = brightness
        row4.brightness = brightness
        row6.brightness = brightness
        pass

    if (not right_was_pressed) and (right_is_pressed):
        print ("right pressed")
        # the right button has been pressed, we should do something ... like change brightness
        brightness += 0.05
        if brightness > 0.3:
            brightness = 0.3
        print ("new brightness = %f" % (brightness))
        row2.brightness = brightness
        row4.brightness = brightness
        row6.brightness = brightness
        pass

    left_was_pressed = left_is_pressed
    right_was_pressed = right_is_pressed

# end of while True loop
