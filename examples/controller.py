# code.py (LumosStick thermal controller example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosStick LEDs, buttons, and tone buzzer.

The LumosStick hardware connects via USB and provides:
    - 280 Neopixel LEDs (when held horizontal it is 7 rows of 40 LEDs each)
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosStick is powered by a Lolin ESP32-S2 which include Wifi connectivity.

The sample code reads a thermocouple and controls a relay.

Documentation: 
**Hardware:**
    * the LumosStick (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import board        # provide the board definitions for IO pins
import digitalio    # used for controlling digital pins
import busio        # access to the SPI interface

import adafruit_fancyled.adafruit_fancyled as fancy # used for generating a color gradient


try:
    from LumosStick import LumosStick, SimpleTimer   # REQUIRED: import the LumosStick library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/LumosStick.py'")
    print ("The latest LumosStick library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/LumosStick")
    print("------------------------------------------------------------------------")



class Probe:
    '''
    The Probe class handles reading data from the thermocouple
    
    The thermocouple is attached to a MAX6675 amplifier board.
    The MAX6675 communicates using SPI. The data is a 12 bit value within a 2-byte response.
    The the 12 bits of measurement start at the 4th bit of the low byte. The top 2 bits of 
    the high byte are not used. Of no reading occurs, the 2 bytes of 0xFF 0xFF.
    
    The Probe exposes an `update()` method along with two properties, `temperature` and `changed`.
    The `update()` method should be call before reading the properties. If there is no valid data 
    available, the `temperature` property will be negative.
    '''

    UPDATE_INTERVAL = 500   # maximum update frequency in milliseconds
    NO_DATA = -1

    def __init__(self):
        self._reading = 0
        self._delta = 0
        self._spi_data = bytearray(2)

        # setup the SPI interface; requires a chip select pin; clock, amd MISO (for data read)
        self._cs = digitalio.DigitalInOut(board.IO8)
        self._cs.direction = digitalio.Direction.OUTPUT
        self._cs.value = True   # CS HIGH = disabled
        self._spi = busio.SPI(board.IO10, MISO=board.IO6)

        while not self._spi.try_lock():
            pass    # WARNING, this could loop forever

        self._spi.configure(baudrate=5000000, phase=0, polarity=0)

        self._timer = SimpleTimer()
        self._timer.interval = Probe.UPDATE_INTERVAL
        self._timer.start()


    def update(self):
        '''
        The Probe `update()` method should be called before reading the properties.
        The update method is rate limited by the `UPDATE_INTERVAL`. This value
        of the interval may be changed but the relatively slow temperature
        change makes faster updates wasteful.

        The results of the `update()` are stored in internal class properties.
        '''
        # only update reading once every 'interval'
        if not self._timer.expired:
            return
        self._timer.start()

        self._delta = 0

        self._cs.value = False              # set chip-select pin low to select the SPI device
        self._spi.readinto(self._spi_data)  # read 2 bytes into our buffer
        self._cs.value = True               # set chip-select pin high to deselect the SPI device

        # if both bytes of 0xFF then there was an error
        if (self._spi_data[0] == 255) and (self._spi_data[1] == 255):
            reading = Probe.NO_DATA
        else:
            reading = (self._spi_data[0] << 8) | (self._spi_data[1])    # combine to make 16 bits
            reading = reading >> 3                                      # first 3 bits are not the reading
            reading = reading >> 2                                      # reading is in 1/4 degrees; divide by 4
            
            '''
            Note: every combination of thermocouple and MAX6675 amplifier will have its own small error.

            Testing method to determine error correct:
                350ml metal vial filled with motor oil, heat gun, digital thermometer, and the thermocouple
                secure heat gun so it is blowing on the metal vial
                hold both the thermometer probe and the thermocouple in the oil (not touching each other or the sides/bottom of the vial)
                heat the oil for 1-3 minute intervals; turn off heat; wait for thermometer to stabilize; record thermometer and thermocouple reading
                repeat (most kitchen digital thermometer probes will only measure to 200C)

            Note: the test must be run for any change of the thermocouple or the SPI interface module.
            '''
            reading -= 3                                                # the error was nearly constant 6C high from 30C to 200C

        self._delta = reading - self._reading
        self._reading = reading
        #print("temperature = %3d " % reading, self.spi_data)


    @property
    def changed(self):
        return self._delta      # amount of change from the previous update cycle

    @property
    def temperature(self):
        return self._reading    # temperature in whole degrees C


class Controller:
    '''
    The Controller class handles the display, buttons, and relay

    '''

    _MAX = 300
    # buttons
    LEFT = 0
    RIGHT = 1

    # button states
    RELEASED = 0
    PRESSED = 1

    def __init__(self):
        self._setting = 100
        self._current = 0
        self._state = False
        
        self._left_timer = SimpleTimer(750)
        self._right_timer = SimpleTimer(750)
        self._input_timer = SimpleTimer(1000)

        self._relay = digitalio.DigitalInOut(board.IO9)
        self._relay.direction = digitalio.Direction.OUTPUT
        self._relay.value = False

        #  gradient starts at green, then yellow at 50% and red at 75%
        self._gradient = [(0.00, 0x003f00), (0.10, 0x003f00), (0.20, 0x2f2f00), (0.50, 0x5f0000)]
        self._palette = fancy.expand_gradient(self._gradient, 60)

        self._lumos = LumosStick(mode=LumosStick.NOCLOCK)
        self._lumos.brightness = 0.15


    def splash(self, text="Reflow Soldering Hotplate"):

        offset = 40
        WAIT = 40   # milliseconds; this is about as fast as it can go
        timer = SimpleTimer(WAIT)
        timer.start()

        # IMPORTANT: the text will look a bit jerky until all of the characters from the font have been loaded
        # one possible kludge would be to render all of the letters without refreshing the display
        for i in range(0, len(text), 6):
            self._lumos.letters(0, text[i:i+6])
        self._lumos.leds(LumosStick.BLACK)

        done = False
        while not done:
            if timer.expired:
                timer.start()
                offset -= 1 # shift to the left
                self._lumos.letters(offset, text)
                self._lumos.update()

                # if the last pixels of the text have scrolls off the left, then we reset and start over again
                if ((len(text) * 6) + offset) < 0:
                    done = True
        # end of while not done loop
        self._prompt("OFF")
        self._set_digits(0, color=None)


    def _prompt(self, text):
        '''
        display a prompt - of up to 3 characters - on the left portion of the LED matrix
        the prompt is followed by a narrow colon glyph
        '''
        length = len(text)
        if (length > 3):
            text = text[:3]
            length = 3

        # clear first part of LED matrix
        for x in range(20):
            for y in range(7):
                self._lumos.led(x, y, LumosStick.BLACK)

        # render actual prompt characters with a colon at the end
        color = LumosStick.BLUE
        if self._state:
            color = LumosStick.RED
        self._lumos.letters(0, text, color)
        pos = (length * 6) + 1
        self._lumos.colon(pos, color)
        

    def _process_buttons(self):
        '''
        Button processing is a bit strange - mostly in order to give an intuitive user experience
        There are 7 possible button states:
            left & right press
            left press
            right press
            left release
            right release
            left hold
            right hold

        The dual-press takes precedence. Since it is likely that the two buttons will nto be perfectly synced,
        one or more button presses may have been already processed and changed the 'setting'. When the 
        dual-press is detected, it will revert the 'setting' to it's most recent value.

        To get the most intuitive user experience, the code tracks a lot of state-changes.

        Rant: A dedicated ON/OFF button would have eliminated a lot of this code.
        '''
        
        initial_setting = self._setting # save the current setting in case we start changing values but its really a double press
        changes = False                 # note if we detect any changes (because it means we changed the prompt)
        toggled = False
        self._input_timer.start(0)

        left_pressed = False
        right_pressed = False
        more = True
        while more:
            more = False

            do_left = False
            do_right = False
            while self._lumos.buttons_changed:
                button, action = self._lumos.button

                if not toggled:
                    if button == LumosStick.LEFT:
                        if  action == True:
                            do_left = True                      # we have a left action
                            left_pressed = True                 # its a button press
                            self._left_timer.start()            # start the 'repeat' timer
                        else:
                            do_left = False
                            left_pressed = False
                            self._left_timer.stop()
                            self._left_timer.interval = 750

                    if button == LumosStick.RIGHT:
                        if action == True:
                            do_right = True                     # we have a right action
                            right_pressed = True                # its a button press
                            self._right_timer.start()           # start the 'repeat' timer
                        else:
                            do_right = False
                            right_pressed = False
                            self._right_timer.stop()
                            self._right_timer.interval = 750

            if not toggled:
                '''
                The 'toggled' state is when we have detected a dual-press. Once this has occurred,
                we dont want to process any button presses as up/down changes.
                '''
                if (left_pressed) and (right_pressed):
                    '''
                    We have just detected a dual-press.
                    '''
                    self._state = not self._state
                    print ("Hotplate is %s" % ("ON" if self._state else "OFF"))
                    self._setting = initial_setting # restore the initial temperature setting
                    self._left_timer.stop()
                    self._right_timer.stop()
                    #self._input_timer.stop()
                    self._left_timer.interval = 750
                    self._right_timer.interval = 750
                    changes = False
                    toggled = True
                    if self._state:
                        self._prompt("ON")
                        self._lumos.tone(440, 0.125)            # lets be cute and play a boop beep
                        self._lumos.tone(523, 0.125)
                    else:
                        self._prompt("OFF")
                        self._lumos.tone(523, 0.125)            # lets be cute and play a beep boop
                        self._lumos.tone(440, 0.125)

                    self._set_digits(self._current)           # ignore any settings changes prior to the dual-press
                    self._lumos.update()
                else:
                    '''
                    If either of the 'repeat' timers has just expired, then consider that a button press
                    '''
                    # support press-and-hold
                    if self._left_timer.expired:
                        do_left = True
                        self._left_timer.interval = 125
                        self._left_timer.start()
                        #print ("left repeat")
                    if self._right_timer.expired:
                        do_right = True
                        self._right_timer.interval = 125
                        self._right_timer.start()
                        #print ("right repeat")

                    '''
                    Process new button presses.
                    '''
                    if do_left or do_right:
                        self._input_timer.start()
                        if not changes:
                            changes = True
                            if self._state:
                                self._prompt("Adj")
                            else:
                                self._prompt("Set")

                        if do_left:
                            self._lumos.tone(440, 0.0625)       # lets be cute and play A3
                            self._setting = (self._setting - 1) if (self._setting > 0) else 0
                        if do_right:
                            self._lumos.tone(523, 0.0625)       # lets be cute and play C3
                            self._setting = (self._setting + 1) if (self._setting < self._MAX) else self._MAX
                        self._set_digits(self._setting, LumosStick.BLUE)
                        self._lumos.update()
                        self._input_timer.start()   # reset the input timer to the beginning
                # end of 'not toggled'

            # We continue to process button presses until our input timer runs out with no presses occurring.
            more = False if self._input_timer.expired else True
        # end of 'more' loop

        if changes:
            if self._state:
                self._prompt("ON")
            else:
                self._prompt("OFF")
            self._set_digits(self._current)
            self._lumos.update()
            #print("button processing finished")

        #return do_left or do_right  # return True if either button is active
        return changes or toggled  # return True if any button actions


    def _set_digits(self, val, color=None):
        # if color is not specified, find color within gradient
        if color is None:
            c = fancy.palette_lookup(self._palette, (val/360.0))
            color = c.pack()
        if (val > 0):
            val = val % 1000
            self._lumos.digits(22, val, 3, color)
        else:
            self._lumos.letters(22, "---", LumosStick.RED)

    '''
    getter/setter for the controller's current temperature value
    '''
    @property
    def temperature(self):
        return self._current
    @temperature.setter
    def temperature(self, val):
        self._set_digits(val)
        self._current = val
        if self._state:
            if self._current < self._setting:
                self._relay.value = True
            else:
                self._relay.value = False

    def update(self):
        '''
        The Controller `update()` method should be called frequently.
        It processes button presses and updates the display.
        '''
        self._process_buttons()
        self._lumos.update()


probe = Probe()             # create the instance of the Probe
controller = Controller()   # create the instance of the Controller

controller.splash()         # not strictly necessary, but fun


'''
The main loop of the code calls update on the probe, checked for new readings, and then updates the controller.
This 
'''
while True:
    probe.update()
    if probe.changed:
        controller.temperature = probe.temperature
    controller.update()
